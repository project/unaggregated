## INTRODUCTION

The unaggregated module allows provides a route which includes non-aggregated

The primary use case for this module is sites which need to provide markup and styles for related sites, without including content or aggregated styles.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Visit /config
- Select the region(s) that should be included in the page response
- Assign the 'access unaggregated content' permission to the relevant roles
- Visit /unaggregated/page

## MAINTAINERS

Current maintainers for Drupal 10:

- Malcolm Young (malcomio) - https://www.drupal.org/u/malcomio

