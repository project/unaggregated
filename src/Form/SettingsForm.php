<?php declare(strict_types = 1);

namespace Drupal\unaggregated\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure unaggregated settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'unaggregated_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['unaggregated.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $theme = \Drupal::config('system.theme')->get('default');
    $regions = system_region_list($theme, $show = REGIONS_ALL);

    $blocks = \Drupal::entityTypeManager()
      ->getStorage('block')
      ->loadByProperties(['theme' => $theme]);;

    // Which regions should be included?
    $config = $this->config('unaggregated.settings');
    $region_settings = $config->get('regions') ?? [];

    $region_options = [];
    foreach ($regions as $key => $value) {
      $region_options[$key] = $value->getUntranslatedString();
    }
    $form['regions'] = [
      '#type' => 'checkboxes',
      '#title' => t('Regions to exclude'),
      '#description' => $this->t('Regions selected here will be excluded from the unaggregated page'),
      '#options' => $region_options,
      '#default_value' => $region_settings,
    ];

    // Which blocks should be included?
    $block_options = [];
    foreach ($blocks as $name => $block) {
      $block_options[$name] = $block->label();
    }
    $block_settings = $config->get('blocks') ?? [];
    $form['blocks'] = [
      '#type' => 'checkboxes',
      '#title' => t('Blocks to exclude'),
      '#description' => $this->t('Blocks selected here will be excluded from the unaggregated page'),
      '#options' => $block_options,
      '#default_value' => $block_settings,
    ];

    $form['absolute'] = [
      '#type' => 'checkbox',
      '#title' => t('Absolute form action'),
      '#description' => t('Should forms on the unaggregated page use absolute URLs for their action?'),
      '#default_value' => $config->get('absolute') ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('unaggregated.settings')
      ->set('regions', $form_state->getValue('regions'))
      ->set('blocks', $form_state->getValue('blocks'))
      ->set('absolute', $form_state->getValue('absolute'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
