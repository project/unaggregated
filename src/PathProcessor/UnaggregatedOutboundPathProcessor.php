<?php

namespace Drupal\unaggregated\PathProcessor;

use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Path processor for unaggregated page.
 */
class UnaggregatedOutboundPathProcessor implements OutboundPathProcessorInterface {

  /**
   * Construct a new UnaggregatedOutboundPathProcessor object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match service.
   */
  public function __construct(protected RouteMatchInterface $routeMatch) {}

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {

    // Are we currently on the unaggregated page? If not, do nothing.
    $route = $this->routeMatch->getRouteName();
    if ($route == 'unaggregated.page') {
      // Set all links to use absolute URLs.
      $options['absolute'] = TRUE;
    }

    return $path;
  }

}
